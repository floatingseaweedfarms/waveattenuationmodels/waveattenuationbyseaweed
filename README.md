# Wave Attenuation by Cultivated Seaweed

A `Python` implementation of the analytical framework addressing the attenuation of both regular and irregular waves propagating over floating seaweed farms.

## Theory

The detailed derivation can be found in the `theory` directory.
There are two Jupyter notebooks available, which are ready for execution.

## Implementation

`Python` scripts to implement the theory are available in the `implementation` directory. Some example scripts are also provided.

## Examples

Several example applications are located in the `examples` directory, including regular/irregular wave attenuation over fixed/flexible canopies.
You are free to tune the parameters and see the effects.
See more details in each example.


## Dependencies

* [Python](https://www.python.org/) (3.9.10)

  Some packages for scientific computing, such as `NumPy`, `SciPy`, `Sympy`, and `Matplotlib`, should also be installed.

## Citation

The releases of this repository are also preserved on [DTU Data](https://doi.org/10.11583/DTU.25563225.v1), where you can select the citation style.

## Publications

1. [Shao, Y., Weiss, M., & Wei, Z. Wave Attenuation by Cultivated Seaweeds: a Linearized Analytical Solution. In Proceedings of 39th International Workshop on Water Waves and Floating Bodies IWWWFB.](https://orbit.dtu.dk/en/publications/wave-attenuation-by-cultivated-seaweeds-a-linearized-analytical-s)

2. [Wei, Z., Weiss, M., Kristiansen, T., Kristiansen, D., & Shao, Y. (2024). Wave attenuation by cultivated seaweeds: A linearized analytical model. Coastal Engineering, 104642.](https://doi.org/10.1016/j.coastaleng.2024.104642)

## References

1. [Dalrymple, R. A., Kirby, J. T., & Hwang, P. A. (1984). Wave diffraction due to areas of energy dissipation. Journal of waterway, port, coastal, and ocean engineering, 110(1), 67-79.](https://doi.org/10.1061/(ASCE)0733-950X(1984)110:1(67))

2. [Kobayashi, N., Raichle, A. W., & Asano, T. (1993). Wave attenuation by vegetation. Journal of waterway, port, coastal, and ocean engineering, 119(1), 30-48.](https://doi.org/10.1061/(ASCE)0733-950X(1993)119:1(30))

3. [Jacobsen, N. G. (2016). Wave-averaged properties in a submerged canopy: Energy density, energy flux, radiation stresses and Stokes drift. Coastal Engineering, 117, 57-69.](https://doi.org/10.1016/j.coastaleng.2016.07.009)

4. [Zhu, L. (2020). Wave Attenuation Capacity of Suspended Aquaculture Structures with Sugar Kelp and Mussels. The University of Maine.](https://digitalcommons.library.umaine.edu/etd/3222/)