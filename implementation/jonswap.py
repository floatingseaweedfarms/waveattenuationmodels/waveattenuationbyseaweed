'''
scripted based on the one provided by Yanlin Shao <yshao@dtu.dk>
'''

from numpy import linspace, pi, zeros, exp, log, sqrt, trapz, errstate
from pm import pierson_moskowitz_spectrum

def jonswap_spectrum(omegas, Hs, Tp=None, Tz=None, gamma=None, sigma_a=0.07, sigma_b=0.09, postresp_compatible=False):
    """
    Returns the JONSWAP spectrum as defined in DNV-RP-C205
    calculated for the input omegas. The gamma parameter is
    calculated by use of :func:`jonswap_gamma` if it is not
    provided by the user. The Tp is calculated by 
    :func:`jonswap_tp` if not provided.
    """
    if gamma is None:
        if Tp is None:
            gamma = jonswap_gamma(Hs, Tz=Tz)
        else:
            gamma = jonswap_gamma(Hs, Tp=Tp)
    
    if Tp is None:
        Tp = jonswap_tp(Tz, gamma)
    
    if postresp_compatible:
        return jonswap_spectrum_postresp(omegas, Hs, Tp, gamma, sigma_a=0.07, sigma_b=0.09, g=9.81)
            
    omega_p = 2 * pi / Tp
    sigma = zeros(len(omegas), float)
    sigma[omegas <= omega_p] = sigma_a
    sigma[omegas > omega_p] = sigma_b
    
    A = 1 - 0.287 * log(gamma)
    PM = pierson_moskowitz_spectrum(omegas, Hs=Hs, Tp=Tp)
    
    # Ignore underflow
    with errstate(under='ignore'):
        S = A * PM * gamma**exp(-0.5 * ((omegas - omega_p) / (sigma * omega_p))**2)
    
    S[omegas == 0] = 0
    
    return S

def jonswap_gamma(Hs, Tp=None, Tz=None):
    """
    Find the JONSWAP peakedness parameter gamma given Hs and Tp
    Uses formulas from DNV-RP-C205
    """
    if Tp is not None:
        # Use formulas from DNV-RP-C205
        q = Tp/sqrt(Hs)
        if q <= 3.6:
            gamma = 5.0
        elif q < 5:
            gamma = exp(5.75 - 1.15 * q)
        else:
            gamma = 1.0
    else:
        # Iterate to find Tp and gamma (should need < 10 iterations)
        Tp = Tz
        for _ in range(100):
            ig  = jonswap_gamma(Hs, Tp)
            iTp = jonswap_tp(Tz, ig)
            if round(Tp, 4) == round(iTp, 4):
                gamma = ig
                break
            Tp = iTp
        else:
            print("gammas", gamma, jonswap_gamma(Hs, Tp))
            print("Tz's", Tz, jonswap_tz(Tp, gamma))
            print("Tp's", Tp, jonswap_tp(Tz, gamma))
            raise ValueError('jonswap_gamma did not converge for Hs=%f, Tz=%s' % (Hs, Tz))
    
    return gamma

def jonswap_tz(Tp, gamma):
    """
    Find the zero up crossing period for JONSWAP given Tp and gamma
    Uses formulas from DNV-RP-C205
    
    Note: for gamma = 3.3 this formulation will return "Tz = 0.778 * Tp"
    and not "Tz = 0.834 * Tp" as the RP says. The value in the RP is wrong
    (ref Øistein Hagen).
    """   
    return (0.6673 + 0.05037 * gamma - 0.006230 * gamma**2 + 0.0003341 * gamma**3) * Tp

def jonswap_tp(Tz, gamma):
    """
    See :func:`jonswap_tz`
    """
    fac = jonswap_tz(1.0, gamma)

    return Tz/fac

def jonswap_spectrum_postresp(omegas, Hs, Tp, gamma, sigma_a=0.07, sigma_b=0.09, g=9.81):
    """
    Implement the JONSWAP spectrum from formulas in the Postresp manual,
    appendix B1.3. This is probably slower than the above direct
    calculation as some integration has to be performed.
    """
    omega_p = 2*pi/Tp

    # Integrate  modified JONSWAP spectrum to find alpha:
    o = linspace(0, 10, 10000)
    s = zeros(len(o), float)
    s[o<omega_p] = sigma_a
    s[o>=omega_p] = sigma_b
    a = exp(-1 / 2 * ((o - omega_p) / (s * omega_p))**2)
    # modified first moment
    f0 = o**-5 * exp(-5 / 4 * (o/omega_p)**-4) * gamma**a * omega_p**4
    f0[0] = 0
    F0 = trapz(x=o, y=f0)
    # modified second moment
    f2 = o**-3 * exp(-5 / 4 * (o/omega_p)**-4) * gamma**a * omega_p**2
    f2[0] = 0
    F2 = trapz(x=o, y=f2)
    # Use integral results for find alpha
    Tz = Tp * sqrt(F0/F2)
    alpha = (F0/F2**2) * (Hs * pi**2 / g / Tz**2)**2
    
    # Calculate the final JONSWAP spectrum:
    sigma = zeros(len(omegas), float)
    sigma[omegas<omega_p] = sigma_a
    sigma[omegas>=omega_p] = sigma_b
    a = exp(-1 / 2*((omegas - omega_p) / sigma / omega_p)**2)
    jonswap = alpha * g**2 * omegas**-5 * exp(-5 / 4 * (omegas / omega_p)**-4) * gamma**a
    jonswap[omegas==0] = 0

    return jonswap