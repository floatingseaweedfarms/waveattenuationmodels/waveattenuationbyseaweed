'''

An example using irregularwavesoverflexiblecanopy.py to predict the wave attenuation over a canopy

Exmaple data is from Jacobsen, Jacobsen, N. G. / McFall, B. C. / van der D.A., A. A frequency distributed dissipation model for canopies
2019-08, Coastal Engineering, Vol.150, p.135-146

'''

import numpy as np
import matplotlib.pylab as plt
from pm import pierson_moskowitz_spectrum
from jonswap import jonswap_spectrum
from canopy import Canopy
from irregularwavesoverflexiblecanopy import IrregularWavesOverFlexibleCanopy

Hs          =   0.037
Tp          =   1.15
wz          =   2 * np.pi / Tp
n_omega     =   300
wmin, wmax  =   0.2 / Tp, 20 / Tp
Tdur        =   3*60*60

# define frequency components in the discretization (of wave spectrum)
omegas      =   np.linspace(wmin, wmax, n_omega)
dw          =   omegas[1] - omegas[0]
omegas_mid  =   0.5 * (omegas[0:n_omega-1] + omegas[1:n_omega])
# define the wave spectrum
Sw_mid      =   jonswap_spectrum(omegas_mid, Hs, Tp=Tp, gamma=3.3)
# wave amplitude ai for the ith wave component
ais = np.sqrt(2 * Sw_mid * dw)

# plot the initial wave spectrum
fig1, ax1 = plt.subplots(1, 1, figsize=(6, 5))
plt.subplots_adjust(left=0.1, right=0.95, top=0.9, bottom=0.15)
ax1.plot(omegas_mid, Sw_mid, 'r-')
ax1.set_xlabel(r'$\omega\,[\mathrm{s^{-1}}]$')
ax1.set_ylabel(r'$S(\omega)\,[\mathrm{m^2/Hz}]$')
ax1.axvline(x=wmin, color='k', linestyle='--')
ax1.axvline(x=wmax, color='k', linestyle='--')
ax1.set_title(r"wave spectrum at $x=0$", fontsize=12)

# water depth
h = 0.685

# canopy
l = h * 0.38 # canopy height
### depth above the canopy (m)
d1 = 0.1
### depth of the canopy (m)
d2 = l
### depth below the canopy (m)
d3 = h - d1 - d2
### stem or blade width (m)
b = 4e-3
### blade thickness (m)
d = 1e-3
### density (1/m^2)
N = 2264
### hydrodynamic coefficients
Cd = 1.95
Cm = 1.00
### density
rhos = 1200
#### canopy
vegetation = Canopy(d1, d2, d3, b, d, rhos, N, Cd, Cm)

# number of integration points in the vertical direction
Nz = 5

model = IrregularWavesOverFlexibleCanopy(vegetation, h, Nz)
model.findlineardamping(omegas_mid, Sw_mid)
print(model.D)
model.findlineardampingself(omegas_mid, Sw_mid)
print(model.D)
_, beta, H_Theta = model.wavenumberanddecaycoeff(omegas_mid, Sw_mid)

# plot the decay coefficients at x=0
fig2, ax2 = plt.subplots(1, 1, figsize=(6, 5))
plt.subplots_adjust(left=0.15, right=0.95, top=0.9, bottom=0.15)
ax2.plot(omegas_mid / 2 / np.pi, np.abs(beta), 'r-')

xmin = 0.2
xmax = 2
ymin = 0
ymax = 0.05
ax2.set_xlim([xmin, xmax])
ax2.set_ylim([ymin, ymax])
ax2.set_xlabel(r'$f\,[\mathrm{s^{-1}}]$')
ax2.set_ylabel(r'$\beta(f)\,[\mathrm{m^{-1}}]$')
ax2.axvline(x=wmin, color='k', linestyle='--')
ax2.axvline(x=wmax, color='k', linestyle='--')
ax2.set_title(r"decay coefficients at $x=0$", fontsize=12)

fig3, ax3 = plt.subplots(1, 1, figsize=(6, 5))
ax3.plot(omegas_mid / 2 / np.pi, H_Theta, '-k')

# decayed wave spectrum at x [m]
x = 7.5
Nx = 10
xx = np.linspace(0, x, Nx)
Sws_new = model.decayedwavespectrum(omegas_mid, Sw_mid, xx, rtol=1.0e-4)
Sw_new = Sws_new[-1]

# plot the decay coefficients at x=5
fig4, ax4 = plt.subplots(1, 1, figsize=(6, 5))
plt.subplots_adjust(left=0.15, right=0.95, top=0.9, bottom=0.15)

ax4.plot(omegas_mid, Sw_mid, '-k', label='initial')
ax4.plot(omegas_mid, Sw_new, '--r', label='present')
ax4.set_xlabel(r'$\omega\,[\mathrm{s^{-1}}]$')
ax4.set_ylabel(r'$S(\omega)\,[\mathrm{m^2/Hz}]$')
ax4.axvline(x=wmin, color='k', linestyle='--')
ax4.axvline(x=wmax, color='k', linestyle='--')
ax4.set_title(r"wave spectrum at $x={:.2f}$ m".format(x), fontsize=12)
ax4.legend()

plt.show()