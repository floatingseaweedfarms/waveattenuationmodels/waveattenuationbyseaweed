'''

Implementation of irregular waves over a flexible canopy modeled by rigid bars 
based on the theory of regular wave over a flexible canopy

'''

import numpy as np
import math
from scipy import integrate
from airywave import AiryWave
from regularwaveoverflexiblecanopy import RegularWaveOverFlexibleCanopy
from constants import rhow, g, rtol, tol

class IrregularWavesOverFlexibleCanopy:
    def __init__(self, canopy, h, Nz):
        self.h  = h                 # water depth
        self.Nz = Nz                # number of integration points in the vertical direction
        self.canopy = canopy        # the canopy

        self.d1 = canopy.d1         # water depth above the canopy
        self.d2 = canopy.d2         # canopy height
        self.d3 = canopy.d3         # water depth below the canopy
        assert math.isclose(self.d1 + self.d2 + self.d3, self.h, abs_tol=tol), "Sum of depths of three layer should be equal to wave depth!"

        # the constant factor of the drag term
        self.D0 = 1 / 2 * canopy.Cd * canopy.b * canopy.N

    def findlineardamping(self, omegas, Sw, alpha=0.7, rtol=rtol):
        assert omegas.size == Sw.size, "INCOMPATIBLE sizes for the inputs!"
        n_omega = omegas.size

        h, d1, d2 = self.h, self.d1, self.d2
        Nz = self.Nz

        # standard deviation of horizontal velocity at the middle position of the canopy
        z0 = -d1 - d2 / 2
        Su = np.zeros(n_omega)
        for i in range(n_omega):
            T = 2 * np.pi / omegas[i]
            wave = AiryWave(h, T)
            Su[i] = np.abs(wave.H_U(z0))**2 * Sw[i]
        sigma_u = np.sqrt(integrate.simpson(Su, omegas))

        # initial guess for the linear damping
        D1 = self.D0 * np.sqrt(8 / np.pi) * sigma_u
        D2 = 0

        # find the converged linear damping
        zz = np.linspace(-d1 - d2, -d1, Nz)
        sigmas_ur = np.zeros(Nz)

        err = 1
        while err > rtol:
            Bv = D1 / self.canopy.N * rhow
            B  = 1 / 3 * Bv * d2**3
            for j in range(Nz):
                z = zz[j]
                Sur = np.zeros(n_omega)
                for i in range(n_omega):
                    omega = omegas[i]
                    T = 2 * np.pi / omega
                    wave = AiryWave(h, T)
                    # H_Theta is calculated from linear wave theory
                    H_Theta = self.canopy.H_Theta_givenlineardamping(wave, Bv)
                    # an abitrary wave height is given here since it is not used
                    model = RegularWaveOverFlexibleCanopy(wave, self.canopy, 1)
                    model.findwavenumber(D1 / omega, H_Theta)
                    Sur[i] = np.abs(model.H_U2(z) - 1j * omega * H_Theta * (-d1 - z))**2 * Sw[i]
                sigmas_ur[j] = np.sqrt(integrate.simpson(Sur, omegas))
            D2 = self.D0 * np.sqrt(8 / np.pi) * integrate.simpson(sigmas_ur**3, zz) / integrate.simpson(sigmas_ur**2, zz)
            err = np.abs((D2 - D1) / D1)
            D1 = alpha * D2 + (1 - alpha) * D1
        
        self.D = D1
    
    def findlineardampingself(self, omegas, Sw, alpha=0.7, rtol=rtol):
        '''
        find H_Theta within the model instead of using linear wave theory
        NOT recommended, since it gives almost the same result but is much slower
        '''
        assert omegas.size == Sw.size, "INCOMPATIBLE sizes for the inputs!"
        n_omega = omegas.size

        h, d1, d2 = self.h, self.d1, self.d2
        Nz = self.Nz

        # standard deviation of horizontal velocity at the middle position of the canopy
        z0 = -d1 - d2 / 2
        Su = np.zeros(n_omega)
        for i in range(n_omega):
            T = 2 * np.pi / omegas[i]
            wave = AiryWave(h, T)
            Su[i] = np.abs(wave.H_U(z0))**2 * Sw[i]
        sigma_u = np.sqrt(integrate.simpson(Su, omegas))

        # initial guess for the linear damping
        D1 = self.D0 * np.sqrt(8 / np.pi) * sigma_u
        D2 = 0

        # find the converged linear damping
        zz = np.linspace(-d1 - d2, -d1, Nz)
        sigmas_ur = np.zeros(Nz)

        err = 1
        while err > rtol:
            Bv = D1 / self.canopy.N * rhow
            B  = 1 / 3 * Bv * d2**3
            for j in range(Nz):
                z = zz[j]
                Sur = np.zeros(n_omega)
                for i in range(n_omega):
                    omega = omegas[i]
                    T = 2 * np.pi / omega
                    wave = AiryWave(h, T)
                    # an abitrary wave height is given here since it is not used
                    model = RegularWaveOverFlexibleCanopy(wave, self.canopy, 1)
                    err1 = 1
                    H1_Theta = self.canopy.H_Theta_givenlineardamping(wave, Bv)
                    H2_Theta = 0
                    while err1 > rtol:
                        model.findwavenumber(D1 / omega, H1_Theta)
                        H2_Theta = self.canopy.H_Theta_givenlineardampingandHU(model.H_U2, Bv, omega)
                        err1 = np.abs((H2_Theta - H1_Theta) / H1_Theta)
                        H1_Theta = H2_Theta
                    H_Theta = H1_Theta
                    Sur[i] = np.abs(model.H_U2(z) - 1j * omega * H_Theta * (-d1 - z))**2 * Sw[i]
                sigmas_ur[j] = np.sqrt(integrate.simpson(Sur, omegas))
            D2 = self.D0 * np.sqrt(8 / np.pi) * integrate.simpson(sigmas_ur**3, zz) / integrate.simpson(sigmas_ur**2, zz)
            err = np.abs((D2 - D1) / D1)
            D1 = alpha * D2 + (1 - alpha) * D1
        
        self.D = D1
    

    def wavenumberanddecaycoeff(self, omegas, Sw, alpha=0.7, rtol=rtol):
        assert omegas.size == Sw.size, "INCOMPATIBLE sizes for the inputs!"
        n_omega = omegas.size

        self.findlineardamping(omegas, Sw, alpha=alpha, rtol=rtol)
        Bv = self.D / self.canopy.N * rhow

        kr = np.zeros(n_omega)
        ki = np.zeros(n_omega)
        Hs_Theta = np.zeros(n_omega)
        for i in range(n_omega):
            omega = omegas[i]
            T = 2 * np.pi / omega
            wave = AiryWave(self.h, T)
            # H_Theta is calculated from linear wave theory
            H_Theta = self.canopy.H_Theta_givenlineardamping(wave, Bv)
            # an abitrary wave height is given here since it is not used
            model = RegularWaveOverFlexibleCanopy(wave, self.canopy, 1)
            model.findwavenumber(self.D / omega, H_Theta)
            kr[i] = np.real(model.k)
            ki[i] = np.imag(model.k)
            Hs_Theta[i] = np.abs(model.H_Theta)
        
        return kr, ki, Hs_Theta

    def decayedwavespectrum(self, omegas, Sw, x, alpha=0.7, rtol=rtol):
        Sws = []

        Sws.append(Sw)
        Nx = x.size
        for i in range(Nx - 1):
            _, ki, _ = self.wavenumberanddecaycoeff(omegas, Sws[i], alpha=alpha, rtol=rtol)
            dx = x[i+1] - x[i]
            Sws.append(Sws[i] * np.exp(2 * ki * dx))
        
        return Sws