import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from canopy import Canopy
from airywave import AiryWave
from constants import rhow, g

h = 0.70 # water depth
T = 1.50 # wave period
H = 0.07 # wave height
wave = AiryWave(h, T)
omega = wave.omega
zeta_a = H / 2

# nondimensionalization
U0 = np.pi * H / T * np.cosh(wave.kh) / np.sinh(wave.kh)
W0 = H / 2 * wave.omega
P0 = rhow * g * H / 2                            # hydrostatic pressure under wave crest at z=0
DP = P0 - rhow * g * H / 2 / np.cosh(wave.kh)    # hydrodynamic pressure crest at z=0

d1 = 0.1 # depth above canopy
d2 = 0.4 # canopy height
d3 = h - d1 - d2 # depth below canopy

b = 0.010 # blade width
d = 0.005 # blade thickness

rhos = 1200
N = 625
Cd = 1.50
Cm = 1.0

vegetation = Canopy(d1, d2, d3, b, d, rhos, N, Cd, Cm)

H_Theta = vegetation.H_Theta(wave, zeta_a)
# H_Theta = 0
print("H_Theta based on linear wave theory:", H_Theta)
print("Theta based on linear wave theory:", H_Theta * zeta_a)