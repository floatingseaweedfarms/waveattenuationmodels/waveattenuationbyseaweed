'''

An example using threelayermodel.py to predict the wave attenuation over a canopy
without the added mass effect

Exmaple data is from Jacobsen, Niels G. / McFall, Brian C. 
Wave-averaged properties for non-breaking waves in a canopy: Viscous boundary layer and vertical shear stress distribution 
2022-06. Coastal Engineering, Vol. 174, p. 104117

Note: not exactly the same values from the paper

'''

import numpy as np
import matplotlib.pyplot as plt
import threelayermodel
from airywave import AiryWave
from canopy import Canopy

## wave characteristic
### water depth (m)
h = 0.7
### wave period (s)
T = 1.5
### wave height (m)
H = 0.07
#### Airy (linear) wave
wave = AiryWave(h, T)
### wave legnth
L = wave.L

## canopy
### depth above the canopy (m)
d1 = 0.263
### depth of the canopy (m)
d2 = 0.437
### depth below the canopy (m)
d3 = 0.0
### stem or blade width (m)
b = 9.5e-3
d = 1.0e-3

rhos = 1200
### density (1/m^2)
N = 625
### hydrodynamic coefficients
Cd = 1.50
Cm = 1.00
#### canopy
vegetation = Canopy(d1, d2, d3, b, d, rhos, N, Cd, Cm)

## three-layer model
model = threelayermodel.ThreeLayerModel(wave, vegetation, H)

## vegetation region
### start (m)
x0 = 0
### end (m)
x1 = 22.25
### discretization
Nx = 100
###
x = np.linspace(x0, x1, Nx)

## solutions
solutionDalrymple = model.solutionDalrymple(x)
solutionKobayashi = model.solutionKobayashi(x)
solutionKobayashiIncremental = model.solutionKobayashiIncremental(x)

## plot
fig, ax = plt.subplots(1, 1, figsize=(10, 4))
plt.subplots_adjust(left=0.1, right=0.95, top=0.9, bottom=0.15)

ax.plot(x / L, solutionDalrymple, '-b', label="Dalrymple-based")
ax.plot(x / L, solutionKobayashi, '-r', label="Kobayashi-based")
ax.plot(x / L, solutionKobayashiIncremental, '--r', label="Kobayashi-based (incremental)")
ax.legend()

xmin = x0 / L
xmax = x1 / L
ymin = 0.4
ymax = 1.1
ax.set_xlim(xmin, xmax)
ax.set_ylim(ymin, ymax)
ax.set_xlabel(r"$x/L$")
ax.set_ylabel(r"$H/H_0$")

plt.show()