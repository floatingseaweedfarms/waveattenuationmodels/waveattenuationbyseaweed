'''

An example using regularwaveoverrigidcanopy.py to predict the wave attenuation over a canopy
with the added mass effect

Three Layer Model is also used to compare the wave attenuation without the added mass effect

Exmaple data is from Jacobsen, Niels G. / McFall, Brian C. 
Wave-averaged properties for non-breaking waves in a canopy: Viscous boundary layer and vertical shear stress distribution 
2022-06. Coastal Engineering, Vol. 174, p. 104117

Note: not exactly the same values from the paper

'''

import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from regularwaveoverrigidcanopy import RegularWaveOverRigidCanopy
from airywave import AiryWave
from canopy import Canopy
from constants import g, rhow

## wave characteristic
### water depth (m)
h = 0.7
### wave period (s)
T = 1.5
### wave height (m)
H = 0.07
#### Airy (linear) wave
wave = AiryWave(h, T)
### wave legnth
L = wave.L

# nondimensionalization
U0 = np.pi * H / T * np.cosh(wave.kh) / np.sinh(wave.kh)
W0 = H / 2 * wave.omega
P0 = rhow * g * H / 2                            # hydrostatic pressure under wave crest at z=0
DP = P0 - rhow * g * H / 2 / np.cosh(wave.kh)    # hydrodynamic pressure crest at z=0

## canopy
### depth above the canopy (m)
d1 = 0.263
### depth of the canopy (m)
d2 = 0.437
### depth below the canopy (m)
d3 = 0.0
### blade width (m)
b = 9.5e-3
### blade thickness (m)
d = 1.0e-3
### blade mass density
rhos = 1200
### density (1/m^2)
N = 625
### hydrodynamic coefficients
Cd = 1.50
Cm = 1.00
#### canopy
vegetation = Canopy(d1, d2, d3, b, d, rhos, N, Cd, Cm)

## three-layer model
model = RegularWaveOverRigidCanopy(wave, vegetation, H)
model.findlineardamping()

## vegetation region
### start (m)
x0 = 0
### end (m)
x1 = 22.25
### discretization
Nx = 20
Nz = 10
###
z1 = np.linspace(-d1, 0, Nz)
z2 = np.linspace(-d1 - d2, -d1, Nz)
z3 = np.linspace(-h, -h + d3, Nz)

x = np.linspace(x0, x1, Nx)
heights, kr, ki = model.waveheights(x)
find_height = interpolate.interp1d(x, heights, kind=3)

## plot wave decay
fig1, ax1 = plt.subplots(1, 1, figsize=(10, 4))
plt.subplots_adjust(left=0.1, right=0.95, top=0.9, bottom=0.15)

ax1.plot(x / x1, heights / H, '-k')

xmin = x0 / x1
xmax = x1 / x1
ymin = 0.4
ymax = 1.1
ax1.set_xlim(xmin, xmax)
ax1.set_ylim(ymin, ymax)
ax1.set_xlabel(r"$x/L$")
ax1.set_ylabel(r"$H/H_0$")

## target location to show velocity and pressure distributions (m)
xx = 10
## wave height here
height = find_height(xx)
model.setH0(height)

## velocities, pressure in each layer
U1 = np.abs(model.H_U1(z1)) * height / 2
U2 = np.abs(model.H_U2(z2)) * height / 2
U3 = np.abs(model.H_U3(z3)) * height / 2
W1 = np.abs(model.H_W1(z1)) * height / 2
W2 = np.abs(model.H_W2(z2)) * height / 2
W3 = np.abs(model.H_W3(z3)) * height / 2
P1 = np.abs(model.H_P1(z1)) * height / 2
P2 = np.abs(model.H_P2(z2)) * height / 2
P3 = np.abs(model.H_P3(z3)) * height / 2

## plot velocities and pressure
fig2, axes2 = plt.subplots(1, 3, figsize=(12, 4), sharey=True)
axes2[0].plot(U1 / U0, z1 / h, '-k')
axes2[0].plot(U2 / U0, z2 / h, '-k')
axes2[0].plot(U3 / U0, z3 / h, '-k')
axes2[1].plot(W1 / W0, z1 / h, '-k')
axes2[1].plot(W2 / W0, z2 / h, '-k')
axes2[1].plot(W3 / W0, z3 / h, '-k')
axes2[2].plot((np.abs(P1) - P0) / DP, z1 / h, '-k')
axes2[2].plot((np.abs(P2) - P0) / DP, z2 / h, '-k')
axes2[2].plot((np.abs(P3) - P0) / DP, z3 / h, '-k')

axes2[0].set_xlim(0, 1)
axes2[0].set_ylim(-1, 0)
axes2[1].set_xlim(0, 1)
axes2[1].set_ylim(-1, 0)
axes2[2].set_xlim(-1.5, 0)
axes2[2].set_ylim(-1, 0)

axes2[0].set_xlabel("$|U|/U_0$")
axes2[1].set_xlabel("$|W|/W_0$")
axes2[2].set_xlabel("$(|P|-P_0)/\Delta P$")
axes2[0].set_ylabel("$z/h$")

# the interface
axes2[0].plot(np.array([0, 1]), np.array([-d1 / h, -d1 / h]), '--k', alpha=0.5)
axes2[1].plot(np.array([0, 1]), np.array([-d1 / h, -d1 / h]), '--k', alpha=0.5)
axes2[2].plot(np.array([-2, 2]), np.array([-d1 / h, -d1 / h]), '--k', alpha=0.5)
axes2[0].plot(np.array([0, 1]), np.array([-(d1 + d2) / h, -(d1 + d2) / h]), '--k', alpha=0.5)
axes2[1].plot(np.array([0, 1]), np.array([-(d1 + d2) / h, -(d1 + d2) / h]), '--k', alpha=0.5)
axes2[2].plot(np.array([-2, 2]), np.array([-(d1 + d2) / h, -(d1 + d2) / h]), '--k', alpha=0.5)

plt.show()