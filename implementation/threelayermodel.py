'''

implementation of the three layer model developed by Zhu, Longhuan
for wave attenuation over a rigid submerged or suspended canopy

References:
    Wave Attenuation Capacity of Suspended Aquaculture Structures with Sugar Kelp and Mussels. 
    The University of Maine, 2020.

'''

import numpy as np
import math
from constants import tol

class ThreeLayerModel:
    def __init__(self, wave, canopy, H):
        self.k0 = wave.k            # wave number without canopy
        self.h  = wave.h            # water depth
        self.omgea = wave.omega     # wave frequency
        self.H0 = H                 # incident wave height

        self.d1 = canopy.d1         # water depth above the canopy
        self.d2 = canopy.d2         # canopy height
        self.d3 = canopy.d3         # water depth below the canopy
        assert math.isclose(self.d1 + self.d2 + self.d3, self.h, abs_tol=tol), "Sum of depths of three layer should be equal to wave depth!"
        self.b  = canopy.b

        self.N  = canopy.N          # distribution density, number of blade per unit area
        self.Cd = canopy.Cd         # drag coefficient
        self.Cm = canopy.Cm         # added mass coefficient

        self.renew()
    
    def renew(self):
        k = self.k0
        d2, d3, h = self.d2, self.d3, self.h

        # decay coefficient
        self.kd = self.Cd * self.b * self.N * k / 9.0 / np.pi           \
            * (9.0 * np.sinh(k * (d2 + d3)) - 9.0 * np.sinh(k * d3)     \
            + np.sinh(3.0 * k * (d2 + d3)) - np.sinh(3.0 * k * d3))     \
            / (np.sinh(k * h) * (2.0 * k * h + np.sinh(2.0 * k * h)))

        self.epsilon = self.Cd * self.b * self.N / 9.0 / np.pi          \
            * (9.0 * np.sinh(k * (d2 + d3)) - 9.0 * np.sinh(k * d3)     \
            + np.sinh(3.0 * k * (d2 + d3)) - np.sinh(3.0 * k * d3))     \
            / (np.sinh(k * h) * (2.0 * k * d2 + np.sinh(2.0 * k * (d2 + d3)) - np.sinh(2.0 * k * d3)))
        
    def dampingcoeff(self, H):
        self.renew()
        return self.epsilon * H

    def decaycoeff(self):
        self.renew()
        return self.kd
    
    def findcompatibledragcoeff(self, kd):
        self.renew()
        return kd / self.kd * self.Cd

    def solutionKobayashi(self, x):
        self.renew()
        return np.exp(-self.kd * self.H0 * x)

    def solutionDalrymple(self, x):
        self.renew()
        return 1.0 / (1.0 + self.kd * self.H0 * x)

    def solutionKobayashiPrognostic(self, x, H):
        self.renew()
        return np.exp(-self.kd * H * x)

    def solutionKobayashiIncremental(self, x):
        self.renew()
        dx = x[1:] - x[:-1]
        heights = np.zeros(x.shape)
        heights[0] = self.H0
        for i in range(x.size - 1):
            heights[i + 1] = self.solutionKobayashiPrognostic(dx[i], heights[i]) * heights[i]
        
        return heights / self.H0