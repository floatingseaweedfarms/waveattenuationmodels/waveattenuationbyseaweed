'''

implementation of analytical model of irregular wave decay by rigid canopy 
presented by Zhu, Longhuan / Huguenard, Kimberly / Zou, Qing-Ping / Fredriksson, David W. / Xie, Dongmei 
    Aquaculture farms as nature-based coastal protection: Random wave attenuation by suspended and submerged canopies 
    2020-09, Coastal Engineering, Vol.160, p.103737.

or specifically, Eq. (21) and (28) from the paper.

'''

import numpy as np
import math
from scipy import integrate
from airywave import AiryWave
from constants import tol

class IrregularWavesDecayByRigidCanopy:
    def __init__(self, canopy, h, Nz):
        self.h  = h                 # water depth
        self.Nz = Nz                # number of integration points in the vertical direction

        self.d1 = canopy.d1         # water depth above the canopy
        self.d2 = canopy.d2         # canopy height
        self.d3 = canopy.d3         # water depth below the canopy
        assert math.isclose(self.d1 + self.d2 + self.d3, self.h, abs_tol=tol), "Sum of depths of three layer should be equal to wave depth!"

        self.b  = canopy.b          # frontal width of canopy stem
        self.N  = canopy.N          # number of canopy stems per unit area
        self.Cd = canopy.Cd         # drag coefficient
        self.Cm = canopy.Cm         # added mass coefficient
    
    def decaycoeff(self, omegas, Sw):
        '''
        return local decay coefficients as function of frequency
        '''
        assert omegas.size == Sw.size, "INCOMPATIBLE sizes for the inputs!"
        n_omega = omegas.size
        dw = omegas[1] - omegas[0]

        h, d1, d2 = self.h, self.d1, self.d2
        b = self.b
        N, Nz = self.N, self.Nz
        Cd = self.Cd

        # standard deviation of the horizontal velocity
        zz = np.linspace(-d1 - d2, -d1, Nz)
        sigma_u = np.zeros(Nz)
        for j in range(Nz):
            Su = np.zeros(n_omega)
            for i in range(n_omega):
                T = 2 * np.pi / omegas[i]
                wave = AiryWave(h, T)
                Su[i] = np.abs(wave.H_U(zz[j]))**2 * Sw[i]
            sigma_u[j] = np.sqrt(integrate.simpson(Su, omegas))
        
        beta = np.zeros(n_omega)
        for i in range(n_omega):
            omega = omegas[i]
            T     = 2 * np.pi / omega
            wave  = AiryWave(h, T)
            k     = wave.k

            Gamma = (np.cosh(k * (zz + h)))**2
            beta[i] = 2 * np.sqrt(2) * N * k * k / (np.sqrt(np.pi) * omega * (2 * k * h + np.sinh(2 * k * h))) * Cd * b * integrate.simpson(sigma_u * Gamma, zz)
        
        return beta
    
    def decayedwavespectrum(self, omegas, Sw, x):
        '''
        return decayed wave spectrum at x
        '''
        beta = self.decaycoeff(omegas, Sw)
        Sw_new = Sw * np.exp(-2 * beta * x)
        
        return Sw_new