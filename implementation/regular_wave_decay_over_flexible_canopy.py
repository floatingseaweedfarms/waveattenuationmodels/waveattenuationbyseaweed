import numpy as np
import matplotlib.pyplot as plt
from scipy import interpolate
from canopy import Canopy
from airywave import AiryWave
from regularwaveoverflexiblecanopy import RegularWaveOverFlexibleCanopy
from constants import rhow, g

h = 0.70 # water depth
T = 1.50 # wave period
H = 0.07 # wave height
wave = AiryWave(h, T)
omega = 2 * np.pi / T

# nondimensionalization
U0 = np.pi * H / T * np.cosh(wave.kh) / np.sinh(wave.kh)
W0 = H / 2 * wave.omega
P0 = rhow * g * H / 2                            # hydrostatic pressure under wave crest at z=0
DP = P0 - rhow * g * H / 2 / np.cosh(wave.kh)    # hydrodynamic pressure crest at z=0

d1 = 0.1 # depth above canopy
d2 = 0.4 # canopy height
d3 = h - d1 - d2 # depth below canopy

b = 0.010 # blade width
d = 0.005 # blade thickness

rhos = 1200
N = 625
Cd = 1.50
Cm = 1.0

vegetation = Canopy(d1, d2, d3, b, d, rhos, N, Cd, Cm)

H_Theta = vegetation.H_Theta(wave, H / 2)
H_Theta = 0
print("H_Theta based on linear wave theory:", H_Theta)
print("Theta based on linear wave theory:", H_Theta * H / 2)
## three-layer model
model = RegularWaveOverFlexibleCanopy(wave, vegetation, H)
model.findlineardampinggivenHTheta(H_Theta)
print("wave number with given H_Theta:", model.k)

model.findlineardamping()
print("wave number with self-determined H_Theta:", model.k)
print("new H_Theta:", model.H_Theta)

## vegetation region
### start (m)
x0 = 0
### end (m)
x1 = 22.25
### discretization
Nx = 20
Nz = 10
###
z1 = np.linspace(-d1, 0, Nz)
z2 = np.linspace(-d1 - d2, -d1, Nz)
z3 = np.linspace(-h, -h + d3, Nz)

x = np.linspace(x0, x1, Nx)
heights, kr, ki = model.waveheights(x)
find_height = interpolate.interp1d(x, heights, kind=3)

## plot wave decay
fig1, ax1 = plt.subplots(1, 1, figsize=(10, 4))
plt.subplots_adjust(left=0.1, right=0.95, top=0.9, bottom=0.15)

ax1.plot(x / x1, heights / H, '-k')

xmin = x0 / x1
xmax = x1 / x1
ymin = 0.4
ymax = 1.1
ax1.set_xlim(xmin, xmax)
ax1.set_ylim(ymin, ymax)
ax1.set_xlabel(r"$x/L_v$")
ax1.set_ylabel(r"$H/H_0$")

## target location to show velocity and pressure distributions (m)
xx = 10
## wave height here
height = find_height(xx)
model.setH0(height)

## velocities, pressure in each layer
U1 = np.abs(model.H_U1(z1)) * height / 2
U2 = np.abs(model.H_U2(z2)) * height / 2
U3 = np.abs(model.H_U3(z3)) * height / 2
W1 = np.abs(model.H_W1(z1)) * height / 2
W2 = np.abs(model.H_W2(z2)) * height / 2
W3 = np.abs(model.H_W3(z3)) * height / 2
P1 = np.abs(model.H_P1(z1)) * height / 2
P2 = np.abs(model.H_P2(z2)) * height / 2
P3 = np.abs(model.H_P3(z3)) * height / 2

## plot velocities and pressure
fig2, axes2 = plt.subplots(1, 3, figsize=(12, 4), sharey=True)
axes2[0].plot(U1 / U0, z1 / h, '-k')
axes2[0].plot(U2 / U0, z2 / h, '-k')
axes2[0].plot(U3 / U0, z3 / h, '-k')
axes2[1].plot(W1 / W0, z1 / h, '-k')
axes2[1].plot(W2 / W0, z2 / h, '-k')
axes2[1].plot(W3 / W0, z3 / h, '-k')
axes2[2].plot((np.abs(P1) - P0) / DP, z1 / h, '-k')
axes2[2].plot((np.abs(P2) - P0) / DP, z2 / h, '-k')
axes2[2].plot((np.abs(P3) - P0) / DP, z3 / h, '-k')

axes2[0].set_xlim(0, 1)
axes2[0].set_ylim(-1, 0)
axes2[1].set_xlim(0, 1)
axes2[1].set_ylim(-1, 0)
axes2[2].set_xlim(-1.5, 0)
axes2[2].set_ylim(-1, 0)

axes2[0].set_xlabel("$|U|/U_0$")
axes2[1].set_xlabel("$|W|/W_0$")
axes2[2].set_xlabel("$(|P|-P_0)/\Delta P$")
axes2[0].set_ylabel("$z/h$")

# the interface
axes2[0].plot(np.array([0, 1]), np.array([-d1 / h, -d1 / h]), '--k', alpha=0.5)
axes2[1].plot(np.array([0, 1]), np.array([-d1 / h, -d1 / h]), '--k', alpha=0.5)
axes2[2].plot(np.array([-2, 2]), np.array([-d1 / h, -d1 / h]), '--k', alpha=0.5)
axes2[0].plot(np.array([0, 1]), np.array([-(d1 + d2) / h, -(d1 + d2) / h]), '--k', alpha=0.5)
axes2[1].plot(np.array([0, 1]), np.array([-(d1 + d2) / h, -(d1 + d2) / h]), '--k', alpha=0.5)
axes2[2].plot(np.array([-2, 2]), np.array([-(d1 + d2) / h, -(d1 + d2) / h]), '--k', alpha=0.5)

plt.show()