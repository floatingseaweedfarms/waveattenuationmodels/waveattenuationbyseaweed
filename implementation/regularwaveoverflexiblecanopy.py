'''

This analytical model is applicable to flexible canopies in small-amplitude motion.
The canopy blade is modeled by a rigid bar pinned at the top.
Small-amplitude motion allows for linearization of the equation of motion.

'''

import numpy as np
import math
from scipy.optimize import fsolve
from scipy.integrate import quad
from constants import g, rhow, rtol, tol

class RegularWaveOverFlexibleCanopy:
    def __init__(self, wave, canopy, H):
        self.k0 = wave.k                # wave number without canopy
        self.h  = wave.h                # water depth
        self.omega = wave.omega         # wave frequency
        self.H  = H                     # incident wave height
        self.U0 = wave.H_U(0) * H / 2   # amplitude of horizontal velocity at z=0

        self.d1 = canopy.d1         # water depth above the canopy
        self.d2 = canopy.d2         # canopy height
        self.d3 = canopy.d3         # water depth below the canopy
        assert math.isclose(self.d1 + self.d2 + self.d3, self.h, abs_tol=tol), "Sum of depths of three layer should be equal to wave depth!"

        self.N = canopy.N
        self.ma_blade = canopy.ma
        self.A_blade = canopy.I + 1 / 3 * self.ma_blade * canopy.l**3
        self.C_blade = (canopy.rhos - rhow) * canopy.As * canopy.l * g * canopy.l / 2

        # the constant factor of the drag term
        self.D0 = 1 / 2 * canopy.Cd * canopy.b * canopy.N
        # added mass divided by water density
        self.AA = canopy.Cm * canopy.N * np.pi / 4 * canopy.b * canopy.b
    
    def kappa_from_k(self, k, D_over_omega):
        A = self.AA
        R1 = (A + 1) / (D_over_omega**2 + (A + 1)**2)
        R2 = D_over_omega / (D_over_omega**2 + (A + 1)**2)
        T1 = 1 / np.sqrt(2) * np.sqrt(R1 + np.sqrt(R1**2 + R2**2))
        T2 = 1 / np.sqrt(2) * R2 / np.sqrt(R1 + np.sqrt(R1**2 + R2**2))

        return (T1 + 1j * T2) * k
    
    def dispersion(self, k0, *args):
        kr, ki = k0
        D_over_omega, H_Theta = args

        k = kr + 1j * ki
        kappa = self.kappa_from_k(k, D_over_omega)
        assert np.abs(kappa) <= np.abs(k), "The norm of kappa should be smaller than that of k. Please check your input!"

        omega, d1, d2, d3 = self.omega, self.d1, self.d2, self.d3

        T1 = np.tanh(k * d1) + np.tanh(k * d3) + kappa / k * np.tanh(kappa * d2) + k / kappa * np.tanh(k * d1) * np.tanh(kappa * d2) * np.tanh(k * d3)
        T2 = 1 + np.tanh(k * d1) * np.tanh(k * d3) + kappa / k * np.tanh(k * d1) * np.tanh(kappa * d2) + k / kappa * np.tanh(kappa * d2) * np.tanh(k * d3)
        H1 = k * d2 * np.sinh(k * d1) * np.cosh(kappa * d2) + kappa * d2 * np.cosh(k * d1) * np.sinh(kappa * d2) + np.cosh(k * d1) \
             - k / kappa * np.sinh(k * d1) * np.sinh(kappa * d2) - np.cosh(k * d1) * np.cosh(kappa * d2)
        H2 = k * d2 * np.cosh(k * d1) * np.cosh(kappa * d2) + kappa * d2 * np.sinh(k * d1) * np.sinh(kappa * d2) + np.sinh(k * d1) \
             - k / kappa * np.cosh(k * d1) * np.sinh(kappa * d2) - np.sinh(k * d1) * np.cosh(kappa * d2)

        T = T1 / T2
        R = omega * omega * (1 + k / kappa / kappa * (self.AA - 1j * D_over_omega) / (1j * (self.AA + 1) + D_over_omega) * H_Theta * (H1 - T * H2)) - g * k * T
        
        return np.real(R), np.imag(R)

    def findwavenumber(self, D_over_omega, H_Theta):
        kr, ki = fsolve(self.dispersion, (self.k0, 0.0), (D_over_omega, H_Theta))

        # check dispersion relation
        np.abs(self.dispersion((kr, ki), D_over_omega, H_Theta)) < tol, "WRONG dispersion relation. Check the wave number!"

        k = kr + 1j * ki
        kappa = self.kappa_from_k(k, D_over_omega)

        d1, d2, d3 = self.d1, self.d2, self.d3
        omega = self.omega

        S  = np.cosh(k * d1) * np.cosh(kappa * d2) * np.cosh(k * d3) + kappa / k * np.sinh(k * d1) * np.sinh(kappa * d2) * np.cosh(k * d3) \
             + k / kappa * np.cosh(k * d1) * np.sinh(kappa * d2) * np.sinh(k * d3) + np.sinh(k * d1) * np.cosh(kappa * d2) * np.sinh(k * d3)
        H2 = k * d2 * np.cosh(k * d1) * np.cosh(kappa * d2) + kappa * d2 * np.sinh(k * d1) * np.sinh(kappa * d2) + np.sinh(k * d1) \
             - k / kappa * np.cosh(k * d1) * np.sinh(kappa * d2) - np.sinh(k * d1) * np.cosh(kappa * d2)
        H_R  = omega * omega / kappa / kappa * (self.AA - 1j * D_over_omega) / (1j * (self.AA + 1) + D_over_omega) * H_Theta
        self.H_A3 = rhow * (g + H_R * H2) / S
        self.H_A2 = self.H_A3 * np.cosh(k * d3) - rhow * k * d2 * H_R
        self.H_B2 = self.H_A3 * k / kappa * np.sinh(k * d3) + rhow * k / kappa * H_R
        self.H_A1 = self.H_A2 * np.cosh(kappa * d2) + self.H_B2 * np.sinh(kappa * d2)
        self.H_B1 = self.H_A2 * kappa / k * np.sinh(kappa * d2) + self.H_B2 * kappa / k * np.cosh(kappa * d2) - rhow * H_R

        self.k = k
        self.kappa = kappa
        self.D_over_omega = D_over_omega
        self.H_R = H_R
        self.H_Theta = H_Theta
    
    def setH0(self, H):
        self.H = H
        self.findlineardamping()
    
    def H_P1(self, z):
        assert np.any(z <= 0.0) and np.any(z >= -self.d1), "z should within the upper layer!"

        return self.H_A1 * np.cosh(self.k * (self.d1 + z)) + self.H_B1 * np.sinh(self.k * (self.d1 + z))
    
    def H_P2(self, z):
        assert np.any(z <= -self.d1) and np.any(z >= -self.d1 - self.d2), "z should within the middle layer!"

        b0 = -rhow * self.k * self.d1 * self.H_R
        b1 = -rhow * self.k * self.H_R

        return self.H_A2 * np.cosh(self.kappa * (self.d1 + self.d2 + z)) + self.H_B2 * np.sinh(self.kappa * (self.d1 + self.d2 + z)) + b1 * z + b0
    

    def H_P3(self, z):
        assert np.any(z <= -self.d1 -self.d2) and np.any(z >= -self.h), "z should within the lower layer!"

        return self.H_A3 * np.cosh(self.k * (self.h + z))
    
    def H_U1(self, z):
        return self.k / rhow / self.omega * self.H_P1(z)
    
    def H_U2(self, z):
        return 1j * self.k / rhow / self.omega / (1j * (self.AA + 1) + self.D_over_omega) * self.H_P2(z)   \
                    + self.omega * (self.AA - 1j * self.D_over_omega) / (1j * (self.AA + 1) + self.D_over_omega) * self.H_Theta * (self.d1 + z)
    
    def H_U3(self, z):
        return self.k / rhow / self.omega * self.H_P3(z)
    
    def H_W1(self, z):
        assert np.any(z <= 0.0) and np.any(z >= -self.d1), "z should within the upper layer!"

        return 1j / rhow / self.omega * self.k * (self.H_A1 * np.sinh(self.k * (self.d1 + z)) + self.H_B1 * np.cosh(self.k * (self.d1 + z)))
    
    def H_W2(self, z):
        assert np.any(z <= -self.d1) and np.any(z >= -self.d1 - self.d2), "z should within the middle layer!"

        return 1j / rhow / self.omega * (self.kappa * (self.H_A2 * np.sinh(self.kappa * (self.d1 + self.d2 + z)) + self.H_B2 * np.cosh(self.kappa * (self.d1 + self.d2 + z))) - rhow * self.k * self.H_R)
    
    def H_W3(self, z):
        assert np.any(z <= -self.d1 -self.d2) and np.any(z >= -self.h), "z should within the lower layer!"

        return 1j / rhow / self.omega * self.k * self.H_A3 * np.sinh(self.k * (self.h + z))

    def lineardamping(self, D, H_Theta):
        D_over_omega = D / self.omega
        self.findwavenumber(D_over_omega, H_Theta)
        d1, d2 = self.d1, self.d2

        # quadratic drag work
        work1, _ = quad(lambda s: np.abs(self.H_U2(-d1 - s) - 1j * self.omega * H_Theta * s)**3, 0, d2)
        # linearized drag work
        work2, _ = quad(lambda s: np.abs(self.H_U2(-d1 - s) - 1j * self.omega * H_Theta * s)**2, 0, d2)

        return self.D0 * 8 / 3 / np.pi * self.H / 2 * work1 / work2
    
    def findlineardampinggivenHTheta(self, H_Theta, rtol=rtol):
        '''
        H_Theta could be calculated from linear wave theory.
        '''
        # initial guess of linear damping
        D1 = self.D0 * self.U0
        D2 = 0

        err = 1
        while err > rtol:
            D2 = self.lineardamping(D1, H_Theta)
            err = np.abs((D2 - D1) / D1)
            D1 = D2

        return D1

    def findlineardamping(self, rtol=rtol):
        '''
        H_Theta is calculated given the coupled flow
        '''
        # initial guess of linear damping
        D0 = self.D0 * self.U0
        D1 = self.lineardamping(D0, 0)
        D2 = 0

        omega = self.omega
        d1, d2 = self.d1, self.d2
        N  = self.N
        ma = self.ma_blade
        A  = self.A_blade
        C  = self.C_blade

        err = 1
        while err > rtol:
            Bv = D1 / N * rhow
            B  = 1 / 3 * Bv * d2**3
            H_Theta = (Bv + 1j * omega * ma) * quad(lambda s: np.abs(self.H_U2(-d1 - s)) * s, 0, d2)[0] / (-omega * omega * self.A_blade + 1j * omega * B + C)
            D2 = self.lineardamping(D1, H_Theta)
            err = np.abs((D2 - D1) / D1)
            D1 = D2

        return D1

    def waveheights(self, x):
        '''
        return the wave height at each x
        '''
        Nx = x.size
        heights = np.zeros(Nx)
        kr = np.zeros(Nx)
        ki = np.zeros(Nx)

        heights[0] = self.H
        for i in range(Nx - 1):
            self.findlineardamping()
            kr[i] = np.real(self.k)
            ki[i] = np.imag(self.k)
            dx = x[i+1] - x[i]
            heights[i+1] = heights[i] * np.exp(ki[i] * dx)
            self.H = heights[i+1]
        
        self.findlineardamping()
        kr[Nx-1] = np.real(self.k)
        ki[Nx-1] = np.imag(self.k)

        return heights, kr, ki