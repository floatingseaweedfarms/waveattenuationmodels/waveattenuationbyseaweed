'''
seaweed canopy modeled by rigid bars pinned at the top
applicable for fixed canopies or flexible canopies in small-amplitude motion
'''

import numpy as np
from scipy.integrate import quad
from constants import g, rhow, rtol

class Canopy:
    def __init__(self, d1, d2, d3, width, thickness, massdensity, distrodensity, Cd, Cm):
        '''
        construct an object of class Canopy
        '''
        self.d1   = d1          # water depth above canopy [m]
        self.d2   = d2          # canopy height [m]
        self.d3   = d3          # water depth below canopy [m]

        self.l = d2             # blade length [m]
        self.b = width          # blade width [m]
        self.d = thickness      # blade thickness [m]

        self.rhos = massdensity # blade mass density [kg/m^3]
        
        self.N = distrodensity  # distribution density, number of blade per unit area

        self.Cd = Cd            # drag coefficient
        self.Cm = Cm            # added mass coefficient

        # cross-section area of a single blade
        self.As = self.b * self.d
        # mass per unit length of a single blade
        self.mu = self.As * self.rhos
        # added mass per unit lenght of a single blade
        self.ma = np.pi / 4 * rhow * self.Cm * self.b * self.b
        # moment of inertia of a single blade
        self.I  = 1 / 3 * self.mu * self.l**3

    def H_Theta(self, wave, zeta_a):
        '''
        find transfer function of pitch motion with given regular linear wave conditions
        
        args:
            wave (AiryWave): an instance of class AiryWave
            zeta_a (float) : wave amplitude
        
        returns:
            float: transfer function of Theta, the pitch angle

        '''

        d1, d2, d3 = self.d1, self.d2, self.d3
        assert wave.h == d1 + d2 + d3, "Sum of depths of three layer should be equal to water depth!"
        h  = d1 + d2 + d3
        z0 = -d1    # where the rigid bar is pinned
        l, b = self.l, self.b

        # wave
        k, omega = wave.k, wave.omega

        # inertia and restoring
        A = self.I + 1 / 3 * self.ma * l**3
        C = (self.rhos - rhow) * self.As * l * g * l / 2

        # integral of H_U from z0 to z0-d2
        K = (l * k * np.sinh(k * d3) + np.cosh(k * d3) - np.cosh(k * (d2 + d3))) / k / k / np.sinh(k * h) * omega
        # initial guess of linear damping at the middle of the canopy
        Bv_0 = 1 / 2 * rhow * self.Cd * b * wave.H_U(-d1 - d2 / 2) * zeta_a

        err = 1
        Bv_1 = Bv_0
        while err > rtol:
            B = 1 / 3 * Bv_1 * l**3     # damping
            H_Theta = -(Bv_1 + 1j * omega * self.ma) * K / (-omega * omega * A + 1j * omega * B + C)
            Bv_2 = 1 / 2 * rhow * self.Cd * b * 8 / 3 / np.pi * zeta_a                                  \
                 * quad(lambda s: np.abs(wave.H_U(z0 - s) - 1j * omega * H_Theta * s)**3, 0, d2)[0]     \
                 / quad(lambda s: np.abs(wave.H_U(z0 - s) - 1j * omega * H_Theta * s)**2, 0, d2)[0]

            err = np.abs((Bv_2 - Bv_1) / Bv_1)
            Bv_1 = Bv_2
        
        return H_Theta
    
    def H_Theta_givenlineardamping(self, wave, Bv):
        '''
        find transfer function of pitch motion with given regular linear wave conditions and linear damping
        
        args:
            wave (AiryWave): an instance of class AiryWave
            Bv (float) : equivalent damping
        
        returns:
            float: transfer function of Theta, the pitch angle

        '''

        d1, d2, d3 = self.d1, self.d2, self.d3
        assert wave.h == d1 + d2 + d3, "Sum of depths of three layer should be equal to water depth!"
        h = d1 + d2 + d3
        l = self.l

        # wave
        k, omega = wave.k, wave.omega

        # inertia and restoring
        A = self.I + 1 / 3 * self.ma * l**3
        C = (self.rhos - rhow) * self.As * l * g * l / 2

        # integral of H_U from z0 to z0-d2
        K = (l * k * np.sinh(k * d3) + np.cosh(k * d3) - np.cosh(k * (d2 + d3))) / k / k / np.sinh(k * h) * omega
        B = 1 / 3 * Bv * l**3     # damping

        return -(Bv + 1j * omega * self.ma) * K / (-omega * omega * A + 1j * omega * B + C)
    
    def H_Theta_givenlineardampingandHU(self, HU, Bv, omega):
        '''
        find transfer function of pitch motion with given regular linear wave conditions and linear damping
        
        args:
            HU (function): transfer function of the amplitude of horizontal velocity
            Bv (float) : equivalent damping
            omega (float): wave angular frequency
            k (complex): wave number
        
        returns:
            float: transfer function of Theta, the pitch angle

        '''

        d1, d2, d3 = self.d1, self.d2, self.d3
        l = self.l

        # inertia and restoring
        A = self.I + 1 / 3 * self.ma * l**3
        C = (self.rhos - rhow) * self.As * l * g * l / 2

        # integral of H_U*s from z0 to z0-d2
        K, _ = quad(lambda s: np.abs(HU(-d1 - s)) * s, 0, d2)
        B = 1 / 3 * Bv * l**3     # damping

        return (Bv + 1j * omega * self.ma) * K / (-omega * omega * A + 1j * omega * B + C)