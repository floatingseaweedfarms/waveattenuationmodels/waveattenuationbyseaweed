import numpy as np
from scipy.optimize import fsolve
from constants import g, rhow

class AiryWave:
    def __init__(self, depth, period):
        self.h = depth
        self.T = period
        self.omega = 2 * np.pi / self.T

        #--- find wave number and wavelength ---#
        ## non-dimensional frequency
        Omega = self.omega * np.sqrt(self.h / g)
        ## solve the non-dimensional dispersion relation
        self.kh, = fsolve(lambda kh, Freq: Freq * Freq - kh * np.tanh(kh), 1.0, args=(Omega))
        ## find wave number and wavelength
        self.k   = self.kh / self.h     # wave number
        self.L   = 2 * np.pi / self.k   # wavelength
    
    def H_Phi(self, z):
        '''
        transfer function of amplitude of velocity potential at z
        '''
        assert np.any(z <= 0.0) and np.any(z >= -self.h), "z should be within water depth!"

        return 1j * g / self.omega * np.cosh(self.k * (z + self.h)) / np.cosh(self.kh)

    def H_P(self, z):
        '''
        transfer function of amplitude of pressure at z
        '''
        return -rhow * 1j * self.omega * self.H_Phi(z)
    
    def H_U(self, z):
        '''
        transfer function of amplitude of horizontal velocity at z
        '''
        return -1j * self.k * self.H_Phi(z)
    
    def H_W(self, z):
        '''
        transfer function of amplitude of vertical velocity at z
        '''
        return 1j * g / self.omega * self.k * np.sinh(self.k * (z + self.h)) / np.cosh(self.kh)