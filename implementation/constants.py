# common constants

g    =    9.81       # gravitational acceleration (kg/s^2)
rhow = 1000.00       # water density (kg/m^3)

rtol = 1.0e-4       # relative tolerance
tol  = 1.0e-10      # absolute tolerance