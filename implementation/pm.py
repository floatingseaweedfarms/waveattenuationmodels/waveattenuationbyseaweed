'''
scripted based on the one provided by Yanlin Shao <yshao@dtu.dk>
'''

from numpy import exp, pi, errstate

def pierson_moskowitz_spectrum(omegas, Hs, Tp=None, Tz=None):
    """
    Returns the PM spectrum as defined in DNV-RP-C205
    calculated for the input omegas 
    """
    if Tp is None:
        Tp = 1.4049 * Tz
    wp = 2 * pi / Tp
    a = (wp / omegas)**4

    # ignore underflow in exp
    with errstate(under = 'ignore'):
        S = 5.0/16 * Hs**2 * a / omegas * exp(-1.25 * a)
    
    S[omegas == 0] = 0

    return S

def pm_tz(Tp):
    """
    Find the zero up crossing period for PM given Tp
    """   
    return Tp / 1.402

def pm_tp(Tz):
    """
    Find the spectral peak period for PM given Tz
    """
    return 1.408 * Tz