'''

An example using irregularwavesdecaybyrigidcanopy.py and irregularwavesoverrigidcanopy.py to predict the wave attenuation over a canopy

Exmaple data is from Jacobsen, Jacobsen, N. G. / McFall, B. C. / van der D.A., A. A frequency distributed dissipation model for canopies
2019-08, Coastal Engineering, Vol.150, p.135-146

'''



import numpy as np
import matplotlib.pylab as plt
from matplotlib import rc
from pm import pierson_moskowitz_spectrum
from jonswap import jonswap_spectrum
from canopy import Canopy
from irregularwavesdecaybyrigidcanopy import IrregularWavesDecayByRigidCanopy
from irregularwavesoverrigidcanopy import IrregularWavesOverRigidCanopy

rc('text', usetex=True)
rc('font', family='serif', size=12)

Hs          =   0.037
Tp          =   1.15
wz          =   2 * np.pi / Tp
n_omega     =   300
wmin, wmax  =   0.2 / Tp, 20 / Tp
Tdur        =   3*60*60

# define frequency components in the discretization (of wave spectrum)
omegas      =   np.linspace(wmin, wmax, n_omega)
dw          =   omegas[1] - omegas[0]
omegas_mid  =   0.5 * (omegas[0:n_omega-1] + omegas[1:n_omega])
# define the wave spectrum
Sw_mid      =   jonswap_spectrum(omegas_mid, Hs, Tp=Tp, gamma=3.3)
# wave amplitude ai for the ith wave component
ais = np.sqrt(2 * Sw_mid * dw)

# plot the initial wave spectrum
fig1, ax1 = plt.subplots(1, 1, figsize=(6, 5))
plt.subplots_adjust(left=0.1, right=0.95, top=0.9, bottom=0.15)
ax1.plot(omegas_mid, Sw_mid, 'r-')
ax1.set_xlabel(r'$\omega\,[\mathrm{s^{-1}}]$')
ax1.set_ylabel(r'$S(\omega)\,[\mathrm{m^2/Hz}]$')
ax1.axvline(x=wmin, color='k', linestyle='--')
ax1.axvline(x=wmax, color='k', linestyle='--')
ax1.set_title(r"wave spectrum at $x=0$", fontsize=12)

# water depth
h = 0.685

# canopy
l = h * 0.38 # canopy height
### depth above the canopy (m)
d1 = h - l
### depth of the canopy (m)
d2 = l
### depth below the canopy (m)
d3 = 0
### stem or blade width (m)
b = 4e-3
d = 1e-3
rhos = 1200
### density (1/m^2)
N = 2264
### hydrodynamic coefficients
Cd = 1.95
Cm = 1.00
#### canopy
vegetation = Canopy(d1, d2, d3, b, d, rhos, N, Cd, Cm)

# number of integration points in the vertical direction
Nz = 5

# two models
model_wo_addedmass = IrregularWavesDecayByRigidCanopy(vegetation, h, Nz)
model_wi_addedmass = IrregularWavesOverRigidCanopy(vegetation, h, Nz)
# decay coefficients at x=0
beta_wo_addedmass = model_wo_addedmass.decaycoeff(omegas_mid, Sw_mid)
# print(beta_wo_addedmass)
_, beta_wi_addedmass = model_wi_addedmass.wavenumanddecaycoeff(omegas_mid, Sw_mid)

# plot the decay coefficients at x=0
fig2, ax2 = plt.subplots(1, 1, figsize=(6, 5))
plt.subplots_adjust(left=0.15, right=0.95, top=0.9, bottom=0.15)
ax2.plot(omegas_mid / 2 / np.pi, beta_wo_addedmass, 'r-', label='Zhu2020')
ax2.plot(omegas_mid / 2 / np.pi, np.abs(beta_wi_addedmass), 'k-', label="present")

xmin = 0.2
xmax = 2
ymin = 0
ymax = 0.05
ax2.set_xlim([xmin, xmax])
ax2.set_ylim([ymin, ymax])
ax2.set_xlabel(r'$f\,[\mathrm{s^{-1}}]$')
ax2.set_ylabel(r'$\beta(f)\,[\mathrm{m^{-1}}]$')
ax2.axvline(x=wmin, color='k', linestyle='--')
ax2.axvline(x=wmax, color='k', linestyle='--')
ax2.set_title(r"decay coefficients at $x=0$", fontsize=12)
ax2.legend()

# decayed wave spectrum at x [m]
x = 100
Sw_new_1 = model_wo_addedmass.decayedwavespectrum(omegas_mid, Sw_mid, x)
Nx = 10
xx = np.linspace(0, x, Nx)
Sws_new_2 = model_wi_addedmass.decayedwavespectrum(omegas_mid, Sw_mid, xx, rtol=1.0e-4)
Sw_new_2 = Sws_new_2[-1]

# plot the decay coefficients at x=5
fig3, ax3 = plt.subplots(1, 1, figsize=(6, 5))
plt.subplots_adjust(left=0.15, right=0.95, top=0.9, bottom=0.15)

ax3.plot(omegas_mid, Sw_mid, '-k', label='initial')
ax3.plot(omegas_mid, Sw_new_1, '--b', label='Zhu2020')
ax3.plot(omegas_mid, Sw_new_2, '--r', label='present')
ax3.set_xlabel(r'$\omega\,[\mathrm{s^{-1}}]$')
ax3.set_ylabel(r'$S(\omega)\,[\mathrm{m^2/Hz}]$')
ax3.axvline(x=wmin, color='k', linestyle='--')
ax3.axvline(x=wmax, color='k', linestyle='--')
ax3.set_title(r"wave spectrum at $x={:.2f}$ m".format(x), fontsize=12)
ax3.legend()

plt.show()