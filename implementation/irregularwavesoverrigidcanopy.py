'''

Implementation of irregular waves over rigid canopy based on the theory of regular wave over rigid canopy.

'''

import numpy as np
import math
from scipy import integrate
from airywave import AiryWave
from regularwaveoverrigidcanopy import RegularWaveOverRigidCanopy
from constants import rtol, tol

class IrregularWavesOverRigidCanopy:
    def __init__(self, canopy, h, Nz):
        self.h  = h                 # water depth
        self.Nz = Nz                # number of integration points in the vertical direction
        self.canopy = canopy        # the canopy

        self.d1 = canopy.d1         # water depth above the canopy
        self.d2 = canopy.d2         # canopy height
        self.d3 = canopy.d3         # water depth below the canopy
        assert math.isclose(self.d1 + self.d2 + self.d3, self.h, abs_tol=tol), "Sum of depths of three layer should be equal to wave depth!"

        self.b  = canopy.b          # frontal width of canopy stem
        self.N  = canopy.N          # number of canopy stems per unit area
        self.Cd = canopy.Cd         # drag coefficient
        self.Cm = canopy.Cm         # added mass coefficient
    
    def findlineardamping(self, omegas, Sw, alpha=0.7, rtol=rtol):
        assert omegas.size == Sw.size, "INCOMPATIBLE sizes for the inputs!"
        n_omega = omegas.size

        h, d1, d2, b = self.h, self.d1, self.d2, self.b
        N, Nz = self.N, self.Nz
        Cd = self.Cd

        # standard deviation of horizontal velocity at the middle position of the canopy
        z0 = -d1 - d2 / 2
        Su = np.zeros(n_omega)
        for i in range(n_omega):
            T = 2 * np.pi / omegas[i]
            wave = AiryWave(h, T)
            Su[i] = np.abs(wave.H_U(z0))**2 * Sw[i]
        sigma_u = np.sqrt(integrate.simpson(Su, omegas))

        # initial guess for the linear damping
        D0 = 1 / 2 * Cd * b * N * np.sqrt(8 / np.pi) * sigma_u

        # find the converged linear damping
        zz = np.linspace(-d1 - d2, -d1, Nz)
        sigmas_u = np.zeros(Nz)

        D1 = D0
        D2 = 0
        while (np.abs((D2 - D1) / D1)) > rtol:
            for j in range(Nz):
                z = zz[j]
                Su = np.zeros(n_omega)
                for i in range(n_omega):
                    omega = omegas[i]
                    T = 2 * np.pi / omega
                    wave = AiryWave(h, T)
                    # an abitrary wave height is given here since it is not used
                    model = RegularWaveOverRigidCanopy(wave, self.canopy, 1)
                    model.findwavenumber(D1 / omega)
                    Su[i] = np.abs(model.H_U2(z))**2 * Sw[i]
                sigmas_u[j] = np.sqrt(integrate.simpson(Su, omegas))
            D2 = D1
            D1 = 1 / 2 * Cd * b * N * np.sqrt(8 / np.pi) * integrate.simpson(sigmas_u**3, zz) / integrate.simpson(sigmas_u**2, zz)
            D1 = alpha * D1 + (1 - alpha) * D2
        
        self.D = D1
    
    def wavenumanddecaycoeff(self, omegas, Sw, alpha=0.7, rtol=rtol):
        assert omegas.size == Sw.size, "INCOMPATIBLE sizes for the inputs!"
        n_omega = omegas.size

        self.findlineardamping(omegas, Sw, alpha=alpha, rtol=rtol)

        kr = np.zeros(n_omega)
        ki = np.zeros(n_omega)
        for i in range(n_omega):
            omega = omegas[i]
            T = 2 * np.pi / omega
            wave = AiryWave(self.h, T)
            # an abitrary wave height is given here since it is not used
            model = RegularWaveOverRigidCanopy(wave, self.canopy, 1)
            model.findwavenumber(self.D / omega)
            kr[i] = np.real(model.k)
            ki[i] = np.imag(model.k)
        
        return kr, ki
    
    def decayedwavespectrum(self, omegas, Sw, x, alpha=0.7, rtol=rtol):
        Sws = []

        Sws.append(Sw)
        Nx = x.size
        for i in range(Nx - 1):
            _, ki = self.wavenumanddecaycoeff(omegas, Sws[i], alpha=alpha, rtol=rtol)
            dx = x[i+1] - x[i]
            Sws.append(Sws[i] * np.exp(2 * ki * dx))
        
        return Sws